<!DOCTYPE html>
<html>
<head>
  <title>Client Form</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="estilos.css">
</head>
<body>

  <form action="insert.php" method="post" autocomplete="off">

    <img src="images/logo.svg" alt="">
    <div class="input-group">

      <div class="input-container">
        <input type="text" name="fullname" placeholder="Full Name">
        <i class="fa-solid fa-user"></i>
      </div>
      <div class="input-container">
        <input type="text" name="email" placeholder="Email ">
        <i class="fa-solid fa-envelope"></i>
      </div>
      <div class="input-container">
        <input type="text" name="city" placeholder="City / Location">
        <i class="fa-solid fa-location-dot"></i>
      </div>
      <div class="input-container">
          <input type="text" name="pqr" placeholder="PQRS">
        <i class="fa-solid fa-sheet-plastic"></i>
      </div>

      <a href="https://drive.google.com/file/d/1LNMG_WbeGifiXAFEP0tQ0rf8MsNVhUHm/view?usp=sharing" target="_blank">DOCUMENTO </a>
 
      <input type="submit" name="bsubmit" class="btn" value="Send Request">
    </div>
  </form>

</body>
</html>

<?php
  include ("insert.php")
?>